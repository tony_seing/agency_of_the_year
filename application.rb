require 'rubygems'
require 'sinatra'
require 'data_mapper'
require 'dm-sqlite-adapter'
require 'open-uri'

class String
  def is_number?
    true if Float(self) rescue false
  end
end
  
  #register Sinatra::Warden
  use Rack::MethodOverride

  helpers do
    def protected!
      unless authorized?
        response['WWW-Authenticate'] = %(Basic realm="Restricted Area")
        throw(:halt, [401, "Not authorized\n"])
      end
    end

    def protected_public_west!
      unless authorized_public_west?
        response['WWW-Authenticate'] = %(Basic realm="Restricted Area")
        throw(:halt, [401, "Not authorized\n"])
      end
    end

     def protected_public_east!
      unless authorized_public_east?
        response['WWW-Authenticate'] = %(Basic realm="Restricted Area")
        throw(:halt, [401, "Not authorized\n"])
      end
    end

    def authorized?
      @auth ||= Rack::Auth::Basic::Request.new(request.env)
      @auth.provided? && @auth.basic? && @auth.credentials && @auth.credentials == ['admin', 'ddbstudio8']
    end

    def authorized_public_west?
      @auth ||= Rack::Auth::Basic::Request.new(request.env)
      @auth.provided? && @auth.basic? && @auth.credentials && (@auth.credentials == ['ddbwest', '2012'])
    end

     def authorized_public_east?
      @auth ||= Rack::Auth::Basic::Request.new(request.env)
      @auth.provided? && @auth.basic? && @auth.credentials && (@auth.credentials == ['ddbnewyork', '2012'])
    end

  end
  DataMapper::setup(:default, "sqlite3://#{Dir.pwd}/aoy.db")

  class Post
    include DataMapper::Resource
    property :id,           Serial    # An auto-increment integer key
    property :name,      	  String   
    property :type,      	  String   
    property :agency,		       String
    property :image_file,		   Text     
    property :video_url,		  Text
    property :non_flash_video_url,      Text

    property :video_image,    Text
    property :is_toc,         Boolean, :default => false
    property :display_index,   Integer, :default => '-1'

    property :link1,           Integer, :default => '-1'
    property :link1_text,      Text
    property :link2,           Integer, :default => '-1'
    property :link2_text,      Text
    property :link3,           Integer, :default => '-1'
    property :link3_text,      Text
    property :link4,           Integer, :default => '-1'
    property :link4_text,      Text
    property :link5,           Integer, :default => '-1'
    property :link5_text,      Text
    property :link6,            Integer, :default => '-1'
    property :link6_text,      Text
   
  end

  DataMapper::Logger.new($stdout, :debug)
  #DataMapper::Model.raise_on_save_failure = true

  DataMapper.finalize
  DataMapper.auto_upgrade!


  get '/' do
    erb :index
  end



  get '/showcase/:agency' do
    if params[:agency] == 'newyork'
      protected_public_east!
    elsif params[:agency] == 'westcoast'
      protected_public_west!
    end

    @posts = Post.all(:agency => params[:agency], :order => [:display_index.asc, :id.asc])
    @post = @posts[0]
    @post_next = @posts[1]
    @post_prev = nil
     
    erb :"showcase/index"
  end

  get '/showcase/:agency/:id' do
    
    if params[:agency] == 'newyork'
      protected_public_east!
    elsif params[:agency] == 'westcoast'
      protected_public_west!
    end

    @post = Post.get(params[:id])
    @post_next = Post.first(:agency => params[:agency], :display_index.gt => @post.display_index, :order => [:display_index.asc, :id.asc])
    @post_prev = Post.last(:agency => params[:agency], :display_index.lt => @post.display_index, :order => [:display_index.asc, :id.asc])

    erb :"showcase/index"
  end

  get '/showcase/:agency/di/:display_index' do
     if params[:agency] == 'newyork'
      protected_public_east!
    elsif params[:agency] == 'westcoast'
      protected_public_west!
    end
    @post = Post.first(:agency => params[:agency], :display_index => params[:display_index])
    @post_next = Post.first(:agency => params[:agency], :display_index.gt => @post.display_index, :order => [:display_index.asc, :id.asc])
    @post_prev = Post.last(:agency => params[:agency], :display_index.lt => @post.display_index, :order => [:display_index.asc, :id.asc])

    erb :"showcase/index"
  end


  get '/posts/new' do
    protected!
    erb :"posts/new"
  end

  get '/posts/edit/:id' do
     protected!
    @post = Post.get(params[:id])

    erb :"posts/edit"
  end


  def make_number(number_string)
    
    if number_string.nil?
      return nil
    elsif number_string.to_s.is_number?
      return number_string.to_i
    else
      return nil
    end
  end


  post '/posts' do
    @post = Post.new(params[:post])

    @post.link1 = make_number(@post.link1)
    @post.link2 = make_number(@post.link2)
    @post.link3 = make_number(@post.link3)
    @post.link4 = make_number(@post.link4)
    @post.link5 = make_number(@post.link5)
    @post.link6 = make_number(@post.link6)
  

    unless params[:post][:image_file].nil?
        puts "post has image file"
        @post.image_file = params[:post][:image_file][:filename]
        begin
          directory = "public/uploads/"
          path = File.join(directory, @post.image_file)
          File.open(path, "wb") { |f| f.write(params[:post][:image_file][:tempfile].read) }
          puts "File uploaded"
        rescue Exception => e
          puts "File NOT uploaded : #{e}"
          return false
        end
    else
        puts "no image_file found"
    end
    


    if @post.save
      puts "post saved"
      redirect '/posts'
    else
      puts "failed to save post: #{@post.errors.inspect}"
      redirect back
    end
  end

  put '/posts/:id' do
    @post = Post.get(params[:id])
    @updated_post = params[:post]
    @post.link1 = make_number(@updated_post['link1'])
    @post.link2 = make_number(@updated_post['link2'])
    @post.link3 = make_number(@updated_post['link3'])
    @post.link4 = make_number(@updated_post['link4'])
    @post.link5 = make_number(@updated_post['link5'])
    @post.link6 = make_number(@updated_post['link6'])
 
    @post.link1_text = @updated_post['link1_text']
    @post.link2_text = @updated_post['link2_text']
    @post.link3_text = @updated_post['link3_text']
    @post.link4_text = @updated_post['link4_text']
    @post.link5_text = @updated_post['link5_text']
    @post.link6_text = @updated_post['link6_text']
     
    
    @post.name =  @updated_post[:name]   
    @post.type = @updated_post[:type]   
    @post.agency = @updated_post[:agency]   
    @post.video_url = @updated_post[:video_url]
        @post.non_flash_video_url = @updated_post[:non_flash_video_url]

    @post.video_image = @updated_post[:video_image]
    
    @post.display_index = @updated_post[:display_index]

    unless params[:post][:image_file].nil?
        puts "post has image file"
        @post.image_file = params[:post][:image_file][:filename]
        begin
          directory = "public/uploads/"
          path = File.join(directory, @post.image_file)
          File.open(path, "wb") { |f| f.write(params[:post][:image_file][:tempfile].read) }
          puts "File uploaded"
        rescue Exception => e
          puts "File NOT uploaded : #{e}"
          return false
        end

    else
        puts "no image_file found"
    end
    
    if @post.save
      puts "post saved: #{@post.inspect}"
      redirect '/posts'
    else
      puts "failed to save post: #{@post.errors.inspect}"
      redirect back
    end

    redirect "/posts"
  end

  get '/posts' do
    protected!
    @posts = Post.all(:order => [:agency.asc, :display_index.asc, :id.asc])
  	erb :"posts/index"
  end

  delete '/posts/:id' do
    @post = Post.get(params[:id]).destroy
    
    redirect '/posts'
  end

